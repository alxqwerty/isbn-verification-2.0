﻿using System;

namespace StringVerification
{
    public static class IsbnVerifier
    {
        public static bool IsValid(string number)
        {
            bool isValidNum = false;

            if (string.IsNullOrWhiteSpace(number))
            {
                throw new ArgumentException("String is null or empty or white space.", number);
            }

            int checkSum = 0;
            int properSymbols = 0;
            int errorSymbols = 0;
            int totalSymbols = 0;
            char[] bookNum = number.ToCharArray();

            if (bookNum.Length < 10)
            {
                return isValidNum;
            }

            for (int i = 10, j = 0; j < bookNum.Length; j++)
            {
                var charConverted = char.GetNumericValue(bookNum[j]);
                int convertedNum = (int)charConverted;
                               
                if (j <= bookNum.Length - 2)
                {
                    var nextCharConverted = char.GetNumericValue(bookNum[j + 1]);
                    int nextConvertedNum = (int)nextCharConverted;

                    if (convertedNum == -1 && nextConvertedNum == -1)
                    {
                        return isValidNum;
                    }
                }
                               
                if (bookNum[^1] == 'X')
                {
                    checkSum += 10;
                    bookNum[^1] = '0';
                }

                if (convertedNum >= 0 && convertedNum <= 9)
                {
                    checkSum += i * convertedNum;
                    i--;
                    properSymbols++;
                    totalSymbols++;
                }
                else
                {
                    errorSymbols++;
                    totalSymbols++;
                    if (errorSymbols > 3)
                    {
                        return isValidNum;
                    }
                }
            }

            if (checkSum % 11 == 0 && properSymbols == 10)
            {
                isValidNum = true;
            }
            else
            {
                isValidNum = false;
            }

            return isValidNum;
        }
    }
}